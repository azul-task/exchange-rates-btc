### All resources are configured according to features available in AWS free tier ###

provider "aws" {
    access_key = var.access_key
    secret_key = var.secret_key
    region = "ap-south-1"
}

#Create VPC
resource "aws_vpc" "azultask_vpc" {
  cidr_block = "10.0.0.0/16"
}

# Create a subnet in azultask_vpc
resource "aws_subnet" "azultask_subnet" {
  vpc_id     = aws_vpc.azultask_vpc.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "1b"
}

# Second subnet as RDS needs atleast two subnets across different availability zones
resource "aws_subnet" "db_subnet" {
  vpc_id     = aws_vpc.azultask_vpc.id
  cidr_block = "10.0.2.0/24"
  availability_zone = "1a"
}

resource "aws_db_subnet_group" "db_group" {
  name = "db_group"
  subnet_ids = [aws_subnet.azultask_subnet, aws_subnet.db_subnet.id]
}

resource "aws_internet_gateway" "azultask_ig" {
  vpc_id = aws_vpc.azultask_vpc.id
}

# Add a route table
resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.azultask_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.azultask_ig.id
  }
}

# Associate the route table with the subnet for public access
resource "aws_route_table_association" "public_rt_sub" {
  subnet_id      = aws_subnet.azultask_subnet.id
  route_table_id = aws_route_table.public_rt.id
}

# Security group to define incoming/outgoing traffic rules
resource "aws_security_group" "azultask_sg" {
  vpc_id = aws_vpc.azultask_vpc.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 8089
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

}

# EC2 Instance
resource "aws_instance" "azultask_instance" {
  ami           = "ami-03bb6d83c60fc5f7c"
  instance_type = "t2.micro"
  key_name      = "azul-key-pair"

  subnet_id                   = aws_subnet.azultask_subnet.id
  vpc_security_group_ids      = [aws_security_group.azultask_sg.id]
  associate_public_ip_address = true
}

# MySQL Db instance
# Configured according to features available in free tier
resource "aws_db_instance" "azultask-mysql-db" {
  allocated_storage = 20
  storage_type = "gp2"
  engine = "mysql"
  engine_version = "8.0.35"
  instance_class = "db.t2.micro"
  username = "admin"
  password = var.db_password
  db_subnet_group_name = aws_db_subnet_group.db_group.name

  skip_final_snapshot = false
}