from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import func
import requests
from datetime import datetime
from apscheduler.schedulers.background import BackgroundScheduler
import pytz
import os
from dotenv import load_dotenv

load_dotenv()
app = Flask(__name__)

# Configure SQLAlchemy
app.config['SQLALCHEMY_DATABASE_URI'] = f'mysql://{os.getenv("DB_USERNAME")}:{os.getenv("DB_PASSWORD")}@{os.getenv("DB_HOST")}/{os.getenv("DB_NAME")}'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

# Defining the  database models
class Currency(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(10), unique=True)

class ExchangeRate(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    currency_id = db.Column(db.Integer, db.ForeignKey('currency.id'))
    target_currency = db.Column(db.String(10))
    exchange_rate = db.Column(db.DECIMAL(38,16))
    timestamp = db.Column(db.DateTime, server_default=func.now(), index=True)

def update_exchange_rates():
    with app.app_context():
        url = 'https://api.coinbase.com/v2/exchange-rates?currency=BTC'

        try:
            response = requests.get(url, timeout=10)
            response.raise_for_status()  # Raise exception for non-2xx status codes

            data = response.json()
            rates = data['data']['rates']

            if rates:
                for target_currency, exchange_rate in rates.items():
                    # Add currency code if it doesn't exist in currency table
                    check_curr = Currency.query.filter_by(code=target_currency).first()
                    if not check_curr:
                        db.session.add(Currency(code=target_currency))
                        db.session.commit()
                    # Add new record to exchange_rate table
                    else:
                        new_rate = ExchangeRate(
                            currency_id=check_curr.id if check_curr else None,
                            target_currency=target_currency,
                            exchange_rate=exchange_rate,
                            timestamp=datetime.now()
                        )
                        db.session.add(new_rate)
                        db.session.commit()

        except requests.exceptions.RequestException as e:
            print(f"Error fetching exchange rates: {e}")

@app.route('/')
def index():
    # Extract latest records for INR & CZK
    inr_rate = ExchangeRate.query.filter(ExchangeRate.target_currency == 'INR').order_by(ExchangeRate.timestamp.desc()).first()
    czk_rate = ExchangeRate.query.filter(ExchangeRate.target_currency == 'CZK').order_by(ExchangeRate.timestamp.desc()).first()
    if czk_rate:
        czk_rate.timestamp = czk_rate.timestamp.astimezone(pytz.timezone('Europe/Berlin')) #Convert timezone for CZK entry
    czk_rate.timezone = 'CET'
    inr_rate.timezone = 'IST'
    latest_rates = [inr_rate, czk_rate]
    return render_template('index.html', rates=latest_rates)

if __name__ == '__main__':
    with app.app_context():
        try:
            Currency.__table__.create(bind=db.engine, checkfirst=True)
            ExchangeRate.__table__.create(bind=db.engine, checkfirst=True)
            if not Currency.query.first():
                default_currency_inr = Currency(code='INR')
                default_currency_czk = Currency(code='CZK')
                db.session.add(default_currency_inr)
                db.session.add(default_currency_czk)
                db.session.commit()

                default_exchange_rate_inr = ExchangeRate(currency_id=default_currency_inr.id, target_currency='INR', exchange_rate=0.00)
                default_exchange_rate_czk = ExchangeRate(currency_id=default_currency_czk.id, target_currency='CZK', exchange_rate=0.00)
                db.session.add(default_exchange_rate_inr)
                db.session.add(default_exchange_rate_czk)
                db.session.commit()
        except Exception as e:
            print(f"Error creating tables: {e}")
        scheduler = BackgroundScheduler()
        scheduler.add_job(update_exchange_rates, 'cron', day_of_week='*', hour=6, minute=0)
        scheduler.start()
    app.run(debug=False, host='0.0.0.0')