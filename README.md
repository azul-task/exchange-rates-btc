# Azul - DevOps Platform Engineer Task 



## Bitcoin Exchange Rates

This project automates the deployment of a Flask application displaying Bitcoin exchange rates to an AWS EC2 instance and a Kubernetes cluster.

For a detailed breakdown of each stage, please refer to the [Project Wiki](https://gitlab.com/azul-task/exchange-rates-btc/-/wikis/home).

This implementation includes features such as:
- Automatic deployment to Kubernetes cluster with zero downtime & automatic rollbacks
- Multi-branch pipeline with conditional triggers
- Custom script to check Gitlab's security reports
- Installation & use of private Gitlab runner
- Configuration of Nginx to host the application

## [Application Development](https://gitlab.com/azul-task/exchange-rates-btc/-/wikis/Application-Development)

- The `app/` directory contains source code of the Flask application that displays Bitcoin Exchange Rates.
- This app connects to a MySQL database to store its data.

## [Infrastructure Provisioning](https://gitlab.com/azul-task/exchange-rates-btc/-/wikis/Infrastructure-Provisioning)

- The `terraform/` directory contains scripts to setup
   - AWS EC2 instance
   - RDS MySQL database

## [GitLab Pipeline](https://gitlab.com/azul-task/exchange-rates-btc/-/wikis/GitLab-Pipeline)

- Multi-branch pipeline (dev, staging, main)
- The jobs that run on each branch differ, but follow this general skeleton:
   - **Test:** To test if code starts up without errors, SAST, Code Quality, Secret Detection
   - **Build:** Build Docker image & push to Container Registry
   - **Deploy:** Pull latest image on required infrastructure & deploy the application
   - **Post-Deploy Test:** Test if application is reachable
   - **Rollback:** Conditionally triggered job, revert to last deployment (One-click Rollbacks are also enabled)
   - **Release:** Create a release tag on successful deployment to main branch

## [EC2 Instance Setup](https://gitlab.com/azul-task/exchange-rates-btc/-/wikis/EC2-Instance-Setup)

- Prepare the VM (set timezone, install Docker).
- Configure Nginx to serve Docker application on port 80.

## [Kubernetes Cluster Setup](https://gitlab.com/azul-task/exchange-rates-btc/-/wikis/Kubernetes-Cluster-Setup) 

- Configure Minikube cluster on my laptop's WSL (configuration files are in `k8s-cluster/`).
- Add a private GitLab runner on my Windows host to setup connectivity between WSL & GitLab.